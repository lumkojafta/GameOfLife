﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace GameOfLife
{
        static class Program
    {
        const int Rows = 50;
        const int Columns = 75;

        static bool runSimulation = true;

        

        public static void Main()
        {
            Random numberGen = new Random();
            var grid = new Status[Rows, Columns];
            RandomNumberGenerator rng = RandomNumberGenerator.Create();
           
            // randomly initialize our grid
            for (var row = 0; row < Rows; row++)
            {
                for (var column = 0; column < Columns; column++)
                {
                    grid[row, column] = (Status)numberGen.Next(0, 2);
                }
            }

            Console.CancelKeyPress += (sender, args) =>
            {
                runSimulation = false;
                Console.WriteLine("\n👋 Ending simulation.");
            };
            
            // let's give our console
            // a good scrubbing
            Console.Clear();

            // Displaying the grid 
            while (runSimulation)
            {
                Print(grid);
                grid = NextGeneration(grid);
            }
        }

        private static Status[,] NextGeneration(Status[,] currentGrid)
        {
            var nextGeneration = new Status[Rows, Columns];

            // Loop through every cell 
            for (var row = 1; row < Rows - 1; row++)
            for (var column = 1; column < Columns - 1; column++)
            {
                // find your alive neighbors
                var aliveNeighbors = 0;
                for (var i = -1; i <= 1; i++)
                {
                    for (var j = -1; j <= 1; j++)
                    {
                        aliveNeighbors += currentGrid[row + i, column + j] == Status.Alive ? 1 : 0;
                    }
                }

                var currentCell = currentGrid[row, column];
                
                // The cell needs to be subtracted 
                // from its neighbors as it was  
                // counted before 
                aliveNeighbors -= currentCell == Status.Alive ? 1 : 0;
  
                // Implementing the Rules of Life 
  
                // Cell is lonely and dies 
                if (currentCell == Status.Alive && aliveNeighbors < 2)
                {
                    nextGeneration[row,column] = Status.Dead;
                }

                // Cell dies due to over population 
                else if (currentCell == Status.Alive && aliveNeighbors > 3)
                {
                    nextGeneration[row, column] = Status.Dead;
                }

                // A new cell is born 
                else if (currentCell == Status.Dead && aliveNeighbors == 3)
                {
                    nextGeneration[row,column] = Status.Alive;
                }
                // stays the same
                else
                {
                    nextGeneration[row, column] = currentCell;
                }
            }
            return nextGeneration;
        }

        private static void Print(Status[,] future, int timeout = 100)
        {
            var stringBuilder = new StringBuilder();
            for (var row = 0; row < Rows; row++)
            {
                for (var column = 0; column < Columns; column++)
                {
                    var cell = future[row, column];
                    stringBuilder.Append(cell == Status.Alive ? "X" : "Y");
                }
                stringBuilder.Append("\n");
            }

            Console.BackgroundColor = ConsoleColor.Black;
            Console.CursorVisible = false;
            Console.SetCursorPosition(0, 0);
            Console.Write(stringBuilder.ToString());
            Thread.Sleep(timeout);
        }
    }
    
    public enum Status
    {
        Dead,
        Alive,
    }

}

